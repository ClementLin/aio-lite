import 'package:flutter/material.dart';
import 'package:banner/banner.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'BB Games'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  int _currentTabIndex = 0;


  @override
  Widget build(BuildContext context) {

    final _kBottomNavBarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('首页')),
      BottomNavigationBarItem(icon: Icon(Icons.chat), title: Text('在线客服')),
      BottomNavigationBarItem(icon: Icon(Icons.atm), title: Text('线上存款')),
      BottomNavigationBarItem(icon: Icon(Icons.card_giftcard), title: Text('我的优惠')),
      BottomNavigationBarItem(icon: Icon(Icons.redeem), title: Text('优惠活动')),
    ];

    final bottomNavBar = BottomNavigationBar(
      items: _kBottomNavBarItems,
      currentIndex: _currentTabIndex,
      type: BottomNavigationBarType.fixed,
      onTap: (int index){
        setState(() {
          _currentTabIndex = index;
        });
      },
    );

    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.menu),
        title: Text(widget.title),
        centerTitle: true,
        backgroundColor: Colors.red,
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            BannerView(
              data: ['Banner A', 'Banner B', 'Banner C'],
              buildShowView: (index, data){
                return Container(
                    margin: EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                        borderRadius: new BorderRadius.all(new Radius.circular(20.0)),
                        border: Border.all(width: 2.0, color: Colors.red)),
                    child: Center(child: Text(data)));
              },
              onBannerClickListener: (index,data){
                print(index);
              }
            ),
            _genBody(_currentTabIndex),
          ],
        ),
      bottomNavigationBar: bottomNavBar,
    );
  }

  _genBody(int index){
    return Expanded(
        child: Container(
            margin: EdgeInsets.all(8.0),
            decoration: BoxDecoration(
                borderRadius: new BorderRadius.all(new Radius.circular(20.0)),
                border: Border.all(width: 2.0, color: Colors.red)),
            child: Center(child: Text('Page $index'))));
  }
}
